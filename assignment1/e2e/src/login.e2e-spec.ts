import { browser, protractor } from 'protractor';
import { LoginPage } from './login.po';
import { DashboardPage } from './dashboard.po';

describe('Login page', () => {

    let page: LoginPage;
    const EC = protractor.ExpectedConditions;

    beforeEach(() => {
        page = new LoginPage();
        browser.get('/login');
    });

    it('should have correct titles and button text', () => {

        expect(page.usernameLabel.getText()).toEqual('Username');
        expect(page.passwordLabel.getText()).toEqual('Password');
        expect(page.signIn.getText()).toEqual('Sign in');

    });
    it ('should display an error message to the user if they provided incorrect credentials', () => {

        page.username.sendKeys('123');
        page.password.sendKeys('123');
        page.signIn.click();
        browser.wait(EC.visibilityOf(page.errorMessage));
        expect(page.errorMessage.getText()).toEqual('Incorrect username or password');

    });
    it ('should redirect the user to the dashboard page if they provided correct credentials', () => {

        const dashboardPage = new DashboardPage();
        const title = 'Welcome to the Dashboard';

        page.username.sendKeys('lokeshjain');
        page.password.sendKeys('123456');
        page.signIn.click();
        browser.wait(EC.visibilityOf(dashboardPage.title));
        expect(dashboardPage.title.isPresent()).toBeTruthy();
        expect(dashboardPage.title.getText()).toContain('Dashboard');

    });


});