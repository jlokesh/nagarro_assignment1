import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {Observable} from 'rxjs';

import { LoginComponent } from './login.component';
//const authServiceSpy = jasmine.createSpyObj('AuthService', ['authorize']);
const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

describe('LoginComponent', () => {
  let component: LoginComponent;
  let authServiceSpy : AuthService;
  let router: Router

  beforeEach(() => {
    authServiceSpy = new AuthService();
  })

  beforeEach(() => {
    component = new LoginComponent(authServiceSpy, routerSpy, new FormBuilder());
    //component = new LoginComponent(null,null);
  });

  function updateForm(userEmail, userPassword) {
    component.loginForm.controls['username'].setValue(userEmail);
    component.loginForm.controls['password'].setValue(userPassword);
  }


  it('should create Login component', () => {
    expect(component).toBeTruthy();
  });

  it('component initial state', () => {
    expect(component.submitted).toBeFalsy();
    expect(component.loginForm).toBeDefined();
    expect(component.loginForm.invalid).toBeTruthy();
    expect(component.error).toBeDefined();
  });

  it('Form invalid should be true when username is blank', (() => {
    updateForm("", "12345");
    component.signIn(component.loginForm);
    expect(component.loginForm.invalid).toBeTruthy();
  }));

  it('Form invalid should be true when password is blank', (() => {
    updateForm("lokeshjain", "");
    component.signIn(component.loginForm);
    expect(component.loginForm.invalid).toBeTruthy();
  }));

  it('User authorization should  return error when user credentials are wrong', (() => {
    updateForm("lokeshjain", "12345");
    spyOn(authServiceSpy,'authorize').and.callFake(() => {
      return new Observable(observer => {
        observer.error('Incorrect username or password');
      })
    })
    component.signIn(component.loginForm);
    expect(component.error).toBeTruthy;
  }));

  it('User authorization should navigate if user authorization is successful', (() => {
    updateForm("lokeshjain", "123456");
    spyOn(authServiceSpy,'authorize').and.callFake(() => {
      return new Observable(observer => {
        observer.next(true);
      })
    })
    component.signIn(component.loginForm);
    expect(component.submitted).toBeTruthy;
    expect(routerSpy.navigateByUrl).toHaveBeenCalled();
  }));

});
