import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error:String ='';
  validationError:String ='';
  loginInProgress:boolean = false;
  loginForm:FormGroup;
  submitted = false;

  constructor(private authService: AuthService, private router: Router,private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
   }

  ngOnInit() {
  }

  signIn(form) {
    this.error = '';
    this.validationError = '';

    console.log(form);
    if(!form.valid){
      this.error = 'Error in Credentials!';
    }

    if(form.controls.username.status === 'INVALID') {
      this.validationError = 'Please enter username';
      return;
    } else if(form.controls.password.status === 'INVALID'){
      this.validationError = 'Please enter password';
      return;
    }

    this.loginInProgress = true;
    this.authService.authorize(form.value.username, form.value.password).subscribe(
      success => {
        this.submitted = true;
        this.router.navigateByUrl('/app/dashboard');
      }, error => {
        this.error = error;
        this.loginInProgress = false;
      }
    );
  }
}
